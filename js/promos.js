
Drupal.promos = {
};
Drupal.promos.loader = function() {
  $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
  $("#TB_overlay").click(tb_remove);
  if(tb_detectMacXFF()){
    $("#TB_overlay").addClass("TB_overlayMacFFBGHack");//use png overlay so hide flash
  }else{
    $("#TB_overlay").addClass("TB_overlayBG");//use background and opacity
  }
  $('body').append("<div id='TB_load'><img src='"+imgLoader.src+"' /></div>");
  $('#TB_load').show();//show loader
}

Drupal.promos.unloader = function() {
  $("#TB_overlay").remove();
  $('#TB_load').remove();
}
/*Drupal.promos.tb_init_original = tb_init;

// new tb_init function - make sure to call reference to original function
Drupal.promos.tb_init = function(domChunk){
	//original function:
	 $(domChunk).click(function(){
  var t = this.title || this.name || null;
  var a = this.href || this.alt;
  var g = this.rel || false;
  tb_show(t,a,g);
  this.blur();
  Drupal.attachBehaviors($('#TB_window'));
  return false;
  });
  //Drupal.promos.tb_init_original(domchunk);
};

// overwrite tb_init with new function
tb_init = Drupal.promos.tb_init;*/

var gPromoId;

//now we can attach behaviors that will get executed on the markup returned in the thickbox window
//Note- jquery.thickbox.js has been modified so Drupal.attachBehaviors is called on it.  I forget why it had to be modified,
//I was probably having difficulty overriding a function...maybe I was just being lazy.
/*
Drupal.behaviors.promosassign = function(context) {
  //alert('works!');
  $('.promos-assign', context).click(function() {
    //close the thickbox
    tb_remove();
    gPromoId = this.rel;
    $('.promoblock').addClass('messages');
    $('.promoblock').addClass('error');
    $('.promoblock').one("click",function() {
      //alert(this.className+' rel:'+gPromoId);
      $(this).append("<div id='TB_load'><img src='"+imgLoader.src+"' /></div>");//add loader to the page
      $('#TB_load').show();//show loader
      $('#'+this.id).load('/promos/assign/'+this.id+'/'+gPromoId,function() {
      //alert('ajax success');
        $('.promoblock').removeClass('messages');
        $('.promoblock').removeClass('error');
      });
    });
    return false;
  });
*/
Drupal.behaviors.promobehaviors = function(context) {
  $('.donthide').show();
  //reattach thickbox links
  tb_init($('a.promothickbox, area.promothickbox, input.promothickbox', context));
  
  //This is the link handler for creation links clicked in the thickbox
  $('.promo-create', context).click(function() {
    var href = this.href;
    if($("#promos-global:checked").length) {
      href += '&global=1';
    }
    location = href;
    return false;
  });
  
  //This is the link handler for assignment links clicked in the thickbox
  $('.promos-assign', context).click(function() {
    //close the thickbox
    tb_remove();
    
    var href, parentid, global;
    parentid = '#'+this.rel;
    //href = this.href+'&destination='+location.pathname;
    href = this.href;
    if($("#promos-global:checked").length) {
      href += '&global=1';
    }
    
    Drupal.promos.loader()
    
    $(parentid).load(href,function() {
      Drupal.attachBehaviors($(parentid));
      Drupal.promos.unloader()
    });
    return false;
  });
  
  $('.promos-remove', context).click(function(event) {
    //@todo - notice this is duplicate code as above..
    var href, parentid;
    href = this.href;
    parentid = '#'+this.rel;
    
    Drupal.promos.loader()
    
    $(parentid).load(href,function() { 
      Drupal.attachBehaviors($(parentid));
      Drupal.promos.unloader()
    });
    return false;
  });
  
  $('.promos-reorder', context).click(function(event) {
    //@todo - notice this is duplicate code as above..
    var href, parentid;
    href = this.href;
    parentid = '#'+this.rel;
    
    Drupal.promos.loader()
    
    $(parentid).load(href,function() { 
      Drupal.attachBehaviors($(parentid));
      Drupal.promos.unloader()
    });
    gPromoId = parentid;
    return false;
  });
  
  var options = { 
    target:        gPromoId,   // target element(s) to be updated with server response 
    beforeSubmit:  function() {
      //$('body').append("<div id='TB_load'><img src='"+imgLoader.src+"' /></div>");
      //$('#TB_load').show();//show loader
      Drupal.promos.loader();
    },
    success: function() {
      $('#TB_load').hide();//show loader
      Drupal.attachBehaviors($(gPromoId));
      Drupal.promos.unloader()
    }
  }; 
  
  // bind form using 'ajaxForm' 
  $('#promos-reorder-form',context).ajaxForm(options); 
  
}


//////////////////////////////////
Drupal.behaviors.resourceLibraries = function(context){
  //hide or unhide child items when a parent is expanded
  $(".toggle a", context).click(function(event){

    var el = $(this.parentNode.parentNode.parentNode.parentNode)
    var next;

    var depth = Number($(el).attr('rel'))+1;

    $('.toggle',this.parentNode.parentNode.parentNode).toggle();
    $('.depth-'+depth,el).toggle();
    /*while(el.next('.depth-'+depth).length) {
      next = el.next('.depth-'+depth);
      $(next).toggle();
      el = next;
    }*/
    return false;
  });
}