
Drupal.behaviors.promocontrolsbehaviors = function(context) {
  $('.promo-item').hover(
    function() {
      var offset = $(this).position();//offset()
      $('.promo-controls',this).css('position','absolute').css('left',offset.left).css('top',offset.top).fadeIn(200);
      //$('.promo-controls',this).fadeIn(200);
    },
    function() {
      $('.promo-controls',this).fadeOut(200);
    }
  );
  
  $('.promoblock').hover(
    function() {
      if($('.donthide',this).length) { return; }
      $('.promos-controls',this).fadeIn(200);
    },
    function() {
      if($('.donthide',this).length) { return; }
      $('.promos-controls',this).fadeOut(200);
    }
  );
  
}